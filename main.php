<?php
require './vendor/autoload.php';

use App\ArrayHelper;
    
$arr = [1, 2, 3, [4, 5, [6, 7, 8], 9], 10]; 

print_r(ArrayHelper::array_flattening_recursive($arr));
print_r(ArrayHelper::array_flattening($arr));
print_r(ArrayHelper::array_flattening_iterative($arr));
$leaves = [];
ArrayHelper::depth_first_flattening_recursive($arr, $leaves);
print_r($leaves);
$leaves = [];
ArrayHelper::depth_first_flattening_recursive([[[1,2],3], 4, [[5, 6], 7]], $leaves);
print_r($leaves);
print_r(ArrayHelper::breadth_first_flattening_iterative([[[1,2],3], 4, [[5, 6], 7]]));

