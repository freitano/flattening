<?php
use PHPUnit\Framework\TestCase;
use App\ArrayHelper;

final class ArrayHelperTest extends TestCase 
{
    protected $testArray = [1, 2, 3, [4, 5, [6, 7, 8], 9], 10];
    protected $expectedResult = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    
    public function testFlatteningRecursiveOk()
    {
        $result = ArrayHelper::array_flattening_recursive($this->testArray);
        $this->assertIsArray($result);
        $this->assertEquals($result, $this->expectedResult);
    }
    
    public function testFlatteningRecursiveEmpty()
    {
        $result = ArrayHelper::array_flattening_recursive([]);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }
    
    public function testFlatteningRecursiveNull()
    {
        $result = ArrayHelper::array_flattening_recursive(null);
        $this->assertNull($result);
    }
    
    public function testFlatteningOk()
    {
        $result = ArrayHelper::array_flattening($this->testArray);
        $this->assertIsArray($result);
        $this->assertEquals($result, $this->expectedResult);
    }
    
    public function testFlatteningEmpty()
    {
        $result = ArrayHelper::array_flattening([]);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }
    
    public function testFlatteningNull()
    {
        $result = ArrayHelper::array_flattening(null);
        $this->assertNull($result);
    }
    
    public function testFlatteningIterativeOk()
    {
        $result = ArrayHelper::array_flattening_iterative($this->testArray);
        $this->assertIsArray($result);
        $this->assertEquals($result, $this->expectedResult);
    }
    
    public function testFlatteningIterativeEmpty()
    {
        $result = ArrayHelper::array_flattening_iterative([]);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }
    
    public function testFlatteningIterativeNull()
    {
        $result = ArrayHelper::array_flattening_iterative(null);
        $this->assertNull($result);
    }
    
    public function testDepthFirstFlatteningRecursive()
    {
        $result = [];
        ArrayHelper::depth_first_flattening_recursive([[[1,2],3], 4, [[5, 6], 7]], $result);
        $this->assertIsArray($result);
        $this->assertEquals($result, [1, 2, 3, 4, 5, 6, 7]);
    }
    
    public function testDepthFirstFlatteningIterative()
    {
        $result = ArrayHelper::depth_first_flattening_iterative([[[1,2],3], 4, [[5, 6], 7]]);
        $this->assertIsArray($result);
        $this->assertEquals($result, [1, 2, 3, 4, 5, 6, 7]);
    }
    
    public function testBreadthFirstFlatteningIterative()
    {
        $result = ArrayHelper::breadth_first_flattening_iterative([[[1,2],3], 4, [[5, 6], 7]]);
        $this->assertIsArray($result);
        $this->assertEquals($result, [4, 3, 7, 1, 2, 5, 6]);
    }
}
