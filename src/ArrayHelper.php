<?php
namespace App;

class ArrayHelper 
{
    /**
     * Funzione per l'appiattimento di array annidati che usa array_walk_recursive.
     * @param array $array
     * @return array
     */
    static function array_flattening_recursive($array) 
    {
        if(!is_array($array)) 
            return null;
        $result = [];
        array_walk_recursive($array, function($v) use (&$result) { 
          $result[] = $v; 
        });
        return $result;
    }
    
    /**
     * Generator function per array_flattening.
     * @param array $array
     */
    static function _array_flatten_generator ($array) 
    {
        foreach ($array as $value) {
            if (is_array($value)) {
                yield from self::_array_flatten_generator($value);
            } else {
                yield $value;
            }
        }
    }
    
    /**
     * Funzione ricorsiva per l'appiattimento di array annidati.
     * @param array $array
     * @return array
     */
    static function array_flattening ($array) 
    {
        if(!is_array($array)) 
            return null;
        $result = self::_array_flatten_generator($array);
        return $result ? iterator_to_array($result, false) : null;
    }
    
    /**
     * Funzione iterativa per l'appiattimento di array annidati.
     * @param array $array
     * @return array
     */
    static function array_flattening_iterative($array)
    {
        $ret = null;
        if(is_array($array)) {
            $ret = [];
            while($array) {
                // estrae il primo elemento della lista
                $elem = array_shift($array);
                if(is_array($elem)) {
                    $index = count($elem);
                    while($index){
                        // inserisce in cima alla lista gli elementi del array annidato
                        array_unshift($array, $elem[--$index]);
                    }
                } else {
                    // inserisce l'elemento trovato nel array risultante
                    array_push($ret, $elem);
                }
            }
        }
        return $ret;
    }
    
    static function depth_first_flattening_recursive($array, &$leaves)
    {
        if(is_array($array)) {
            foreach ($array as $elem) {
                self::depth_first_flattening_recursive($elem, $leaves);
            }
        } else $leaves[] = $array;
    }
    
    static function depth_first_flattening_iterative($array)
    {
        $ret = null;
        if(is_array($array)) {
            $ret = [];
            while($array) {
                // estrae il primo elemento della lista
                $elem = array_shift($array);
                if(is_array($elem)) {
                    $index = count($elem);
                    while($index){
                        // inserisce in CIMA alla lista gli elementi del array annidato
                        array_unshift($array, $elem[--$index]);
                    }
                } else {
                    // inserisce l'elemento trovato nel array risultante
                    array_push($ret, $elem);
                }
            }
        }
        return $ret;
    }
    
    static function breadth_first_flattening_iterative($array)
    {
        $ret = null;
        if(is_array($array)) {
            $ret = [];
            while($array) {
                // estrae il primo elemento della lista
                $elem = array_shift($array);
                if(is_array($elem)) {
                    $index = 0;
                    $count = count($elem);
                    while($index < $count) {
                        // inserisce in CODA alla lista gli elementi del array annidato
                        array_push($array, $elem[$index++]);
                    }
                } else {
                    // inserisce l'elemento trovato nel array risultante
                    array_push($ret, $elem);
                }
            }
        }
        return $ret;
    }
    
}
    