# Array Flattening
The ArrayHelper class contains useful functions to manipulate arrays.

  - array_flattening_recursive: recursive function that transforms a nested array of integers in a flat one.
  - array_flattening: transforms a nested array of integers in a flat one, but using a generator function.

## How to use

The functions implemented are static, so you can directly call:

    $flattenArray = ArrayHelper::array_flattening(<input-nested-array>);

You can find an example inside main.php file.

## How to test

PHPUnit is used for testing. To test the functions inside the class use the following commands:

    ./vendor/bin/phpunit tests
